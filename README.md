# Jitesoft.Commands

This package contains base interface and classes to create command classes which will be managed by the Jitesoft.Commands.Manager.  
The intended usage is to pass the arguments from the `main` method as a list into the `manager.Invoke(...)` method for it to parse incomming CLI commands.

Each command need to implement the `ICommand` interface, when created it should specify the arguments and optional flags. The name will be used upon handling the invokation.  
When a command is called, its `HandleAsync` method will be invoked. The manager expects this to be an async operation and will await the result. The result is expected to be of string value.  
The parameters passed to the `HandleAsync` method is two lists, the first one contains all arguments that where passed and the second all the options. The objects uses the same classes as the defined arguments and options in the
command implementation, but with the values set in case of a value.

## Example

Invoking the manager from main method.

```csharp
class Program {
  
  int main(string[] args) {
    string result = manager.Invoke(args.ToList()).GetAwaiter().GetResult();

    Console.Write(result);
    Console.ReadKey(true);
  }

}
```

More examples will come later.

## License

MIT License

Copyright (c) 2018 Jitesoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
