﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Jitesoft.Commands
{
    /// <summary>
    /// Class which stores and invokes commands based on their signature and arguments.
    /// </summary>
    public class Manager
    {
        private readonly IList<ICommand> commands;

        /// <summary>
        /// Read only list of commands registered to the manager.
        /// </summary>
        public IReadOnlyList<ICommand> Commands => commands.ToList().AsReadOnly();

        /// <summary>
        /// Register a command for the manager to handle.
        /// </summary>
        /// <param name="command">Command to register.</param>
        public void RegisterCommand(ICommand command)
        {
            if (commands.Any(cmd => cmd.Name == command.Name))
            {
                throw new ArgumentException($"A command with the name '{command.Name}' already exist.");
            }

            commands.Add(command);
        }

        /// <summary>
        /// Get the current number of registered commands in the manager.
        /// </summary>
        /// <returns>Number of registered commands.</returns>
        public int CommandCount()
        {
            return commands.Count;
        }

        /// <summary>
        /// Creates a new command manager instance.
        /// </summary>
        public Manager()
        {
            commands = new List<ICommand>();
        }

        /// <summary>
        /// Takes a list of arguments (passed from main method prefferably), matches the arguments
        /// with the correct command (if any) and invokes it with provided flags and args.
        /// Resulting string value matches the one returned by the command handle method.
        /// </summary>
        /// <param name="args">Arguments from the main method.</param>
        /// <returns>Value created from command invokation.</returns>
        public async Task<string> Invoke(IList<string> args)
        {
            // Arguments passed in to the invoke method will contain of the following:
            // [0] = Command name
            // Arguments, which are not using a seperator
            // Options, which are using a seperator (either a space or a = sign)
            
            var command = commands.First(c => string.Equals(c.Name, args[0], StringComparison.OrdinalIgnoreCase));
            args.RemoveAt(0);

            // Select all which are actually options.
            IList<string> strOptions = args.Where(x => x.StartsWith("-")).ToList();
            IList<string> strArgs = args.Where(x => !x.StartsWith("-")).ToList();

            int requiredCount = command.Arguments.Count(x => !x.IsOptional);
            int optionalCount = command.Arguments.Count(x => x.IsOptional);

            if (strArgs.Count < requiredCount || strArgs.Count > (requiredCount + optionalCount))
            {
                var expectedCount = optionalCount == 0
                    ? $"{command.Arguments.Count}"
                    : $"{requiredCount} - {requiredCount + optionalCount}";

                throw new ArgumentException($"Invalid number of arguments. Command expected {expectedCount} arguments but recieved {strArgs.Count}.");
            }

            IList<Option> options = strOptions.Select(o => GetOption(o, command)).ToList();
            IList<Argument> arguments = new List<Argument>(command.Arguments.Count);
            for (var i = 0; i < strArgs.Count; i++)
            {
                arguments.Add(GetArgument(strArgs[i], command.Arguments[i]));
            }
            
            return await command.HandleAsync(arguments, options);
        }

        private static Option GetOption(string option, ICommand command)
        {
            // Options can be either prefixed with - or --, we just remove all -.
            option = option.Replace("-", "");
            var optionName = option;
            // If the option have a `=` we should split it up, then the first value is the name and the second is the value.
            if (option.Contains('='))
            {
                optionName = option.Split('=')[0];
                option = option.Split('=')[1];

            }

            var optionAsObj = command.Options.FirstOrDefault(o => o.Name == optionName || o.Alternatives.Contains(optionName));
            if (optionAsObj == default(Option))
            {
                throw new ArgumentException($"Invalid command signature, unknown option '{optionName}'. Expected: {GetSignature(command)}");
            }

            if (optionAsObj.ExpectedValueType != null)
            {
                if (option == string.Empty || (option == optionName))
                {
                    throw new ArgumentException(
                        $"Unexpected value of option. Expected value of '{optionName}' to be of type {optionAsObj.ExpectedValueType.Name}. Value was empty."
                    );
                }

                var tc = TypeDescriptor.GetConverter(optionAsObj.ExpectedValueType);
                try
                {
                    optionAsObj.Value = tc.ConvertFromString(option);
                }
                catch (Exception exception)
                {
                    throw new ArgumentException($"Invalid option value. Expected value of '{optionAsObj.Name}' to be of type {optionAsObj.ExpectedValueType.Name}.", exception);
                }
            }

            return optionAsObj;
        }

        private static Argument GetArgument(string argument, Argument arg)
        {
            if (argument.Contains('"'))
            {
                argument = argument.Replace("\"", "");
            }


            if (arg.ExpectedValueType != null)
            {
                var tc = TypeDescriptor.GetConverter(arg.ExpectedValueType);
                try
                {
                    arg.Value = tc.ConvertFromString(argument);
                }
                catch (Exception exception)
                {
                    throw new ArgumentException($"Invalid argument value. Expected value of {arg.Name} to be of type {arg.ExpectedValueType.Name}.", exception);
                }
            }

            arg.Value = argument;
            return arg;
        }

        /// <summary>
        /// Get a command signature as string.
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Command signature as string.</returns>
        public static string GetSignature(ICommand command)
        {
            var optional = command.Options.Select(o =>
            {
                IList<string> value = o.Alternatives.Select(a => a.Length > 1 ? $"--{a}" : $"-{a}").ToList();
                var alts = value.Count > 0 ? $"{string.Join("|", value)}" : "";
                return "{" + (o.Name.Length > 1 ? $"--{o.Name}" : $"-{o.Name}") + (alts == "" ? "" : $"[{alts}]") + (o.ExpectedValueType == null ? "" : $" {o.ExpectedValueType.Name}") + "}";
            }).ToList();

            var required = command.Arguments.Select(a => $"<{a.Name}>").ToList();

            var args = required.Count > 0 ? string.Join(" ", required) : "";
            var opts = optional.Count > 0 ? string.Join(" ", optional) : "";

            // command argument1 argument2 --option1 --option2
            return $"{command.Name} {args} {opts}";
        }
    }
}
