﻿using System;
using System.Collections.Generic;

namespace Jitesoft.Commands
{
    /// <summary>
    /// Option class.
    /// An option is basically a flag.
    /// </summary>
    public class Option
    {
        /// <summary>
        /// Name of the option, used as the default flag.
        /// (`name` will be used as `--name` and `n` will be used as `-n`)
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Alternative flags for the name.
        /// </summary>
        public IList<string> Alternatives { get; set; } = new List<string>();

        /// <summary>
        /// Value expected from the argument.
        /// This can be left null, if so, the flag does not require any extra argument.
        /// </summary>
        public Type ExpectedValueType { get; set; } = null;

        /// <summary>
        /// Value if any.
        /// </summary>
        public object Value { get; internal set; } = null;

        /// <summary>
        /// Description of the flag.
        /// </summary>
        public string Description { get; set; } = string.Empty;
    }
}
