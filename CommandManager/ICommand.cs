﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jitesoft.Commands
{
    /// <summary>
    /// Command interface that all commands should implement.
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// Arguments used in the command.
        /// Depending on the order they are added they will be expected
        /// to be entered in the command invokation.
        /// </summary>
        IList<Argument> Arguments { get; }

        /// <summary>
        /// Flags used by the command.
        /// </summary>
        IList<Option> Options { get; }

        /// <summary>
        /// Name of the command.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Description of the command.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Handle command invokation.
        /// </summary>
        /// <param name="args">Supplied arguments.</param>
        /// <param name="options">Supplied options.</param>
        /// <returns>Async task.</returns>
        Task<string> HandleAsync(IList<Argument> args, IList<Option> options);
    }
}
