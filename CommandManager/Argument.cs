﻿using System;

namespace Jitesoft.Commands
{
    /// <summary>
    /// Argument class.
    /// An argument is one of the values which comes after the command name.
    /// </summary>
    public class Argument
    {
        /// <summary>
        /// Name of the argument.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Value.
        /// Used when the argument is passed to the handler.
        /// </summary>
        public object Value { get; internal set; } = null;

        /// <summary>
        /// Description of the argument.
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// Type that the value is expected to be.
        /// If not set, string.
        /// </summary>
        public Type ExpectedValueType { get; set; } = null;

        /// <summary>
        /// If this is an optional argument.
        /// Optional arguments have to be added after none-optional arguments, or the manager will
        /// confuse them with the none-optional.
        /// </summary>
        public bool IsOptional { get; set; } = false;
    }
}
