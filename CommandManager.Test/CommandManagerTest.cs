﻿using System;
using System.Collections.Generic;
using Jitesoft.Commands;
using Test.TestObjects;
using Xunit;

namespace Test
{
    public class CommandManagerTest : IDisposable
    {
        public CommandManagerTest()
        {
            manager = new Manager();
        }

        public void Dispose()
        {
            manager = null;
        }

        private Manager manager;

        [Fact]
        public void TestRegister()
        {
            manager.RegisterCommand(new BasicCommand("Test", "abc123"));
            Assert.Equal(1, manager.CommandCount());
            manager.RegisterCommand(new BasicCommand("Test2", "abc123"));
            Assert.Equal(2, manager.CommandCount());
        }

        [Fact]
        public void TestRegisterFailNameAlreadyExists()
        {
            manager.RegisterCommand(new BasicCommand("Test", "abc123"));
            Assert.Equal(1, manager.CommandCount());

            var exception = Assert.Throws<ArgumentException>(() =>
            {
                manager.RegisterCommand(new BasicCommand("Test"));
            });

            Assert.Equal("A command with the name 'Test' already exist.", exception.Message);
        }
        
        [Fact]
        public void TestGetSignature()
        {
            var cmd = new BasicCommand("test", "description", new List<Argument>
            {
                new Argument {Name = "arg1"},
                new Argument {Name = "arg2"}
            }, new List<Option>
            {
                new Option
                {
                    Name = "opt1",
                    Alternatives = new List<string> {"o", "o1", "a"},
                    Description = "description",
                    ExpectedValueType = typeof(int)
                },
                new Option {Name = "opt2"}
            });

            var expected = "test <arg1> <arg2> {--opt1[-o|--o1|-a] Int32} {--opt2}";
            Assert.Equal(expected, Manager.GetSignature(cmd));
        }

        [Fact]
        public async void TestInvoke()
        {
            BasicCommand cmd = new BasicCommand("test", "test description", new List<Argument>()
            {
                new Argument() { Name = "argument1", ExpectedValueType = typeof(string)}, new Argument() { Name = "argument2" }
            }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            bool called = false;
#pragma warning disable 1998
            cmd.Callback = async delegate(IList<Argument> args, IList<Option> options)
#pragma warning restore 1998
            {
                Assert.Equal(2, args.Count);
                Assert.Equal(2, options.Count);

                Assert.Equal("abc123", args[0].Value);
                Assert.Equal("argument1", args[0].Name);

                Assert.Equal("efg456 hej hej hej!", args[1].Value);
                Assert.Equal("argument2", args[1].Name);

                Assert.Null(options[0].Value);
                Assert.Equal("option1", options[0].Name);

                Assert.NotNull(options[1].Value);
                Assert.Equal(123, options[1].Value);
                Assert.Equal("option2", options[1].Name);
                called = true;

                return "Yay, it works!";
            };

            manager.RegisterCommand(cmd);

            var result = await manager.Invoke(new List<string>()
            {
                "test",
                "abc123",
                "\"efg456 hej hej hej!\"",
                "-p",
                "--option2=123"
            });

            Assert.Equal("Yay, it works!", result);
            Assert.True(called);

        }

        [Fact]
        public async void TestInvokeInvalidArgCountOverfolow()
        {
            ICommand cmd = new BasicCommand("test", "test description", new List<Argument>()
            {
                new Argument() { Name = "argument1", ExpectedValueType = typeof(string)}, new Argument() { Name = "argument2" }
            }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            var result = await Assert.ThrowsAsync<ArgumentException>(async () => await manager.Invoke(new List<string>()
            {
                "test",
                "abc123",
                "efg",
                "hij",
                "-o"
            }));

            Assert.Equal("Invalid number of arguments. Command expected 2 arguments but recieved 3.", result.Message);
        }

        [Fact]
        public async void TestInvokeInvalidArgCountUnderflow()
        {
            ICommand cmd = new BasicCommand("test", "test description", new List<Argument>()
            {
                new Argument() { Name = "argument1", ExpectedValueType = typeof(string)}, new Argument() { Name = "argument2" }
            }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            var result = await Assert.ThrowsAsync<ArgumentException>(async () => await manager.Invoke(new List<string>()
            {
                "test",
                "abc123",
                "-o"
            }));

            Assert.Equal("Invalid number of arguments. Command expected 2 arguments but recieved 1.", result.Message);

        }

        [Fact]
        public async void TestInvokeWithOptionalArguments()
        {
            BasicCommand cmd = new BasicCommand("test", "test description", new List<Argument>()
            {
                new Argument() { Name = "argument1", ExpectedValueType = typeof(string)}, 
                new Argument() { Name = "argument2", IsOptional = true}
            }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            bool called = false;
#pragma warning disable 1998
            cmd.Callback = async delegate (IList<Argument> args, IList<Option> options)
#pragma warning restore 1998
            {
                Assert.Equal(1, args.Count);

                Assert.Equal("argument1", args[0].Name);
                Assert.Equal("abc123", args[0].Value);
                called = true;
                return "Yay, it works!";
            };


            var result = await manager.Invoke(new List<string>()
            {
                "test",
                "abc123"
            });

            Assert.Equal("Yay, it works!", result);
            Assert.True(called);

            called = false;
#pragma warning disable 1998
            cmd.Callback = async delegate (IList<Argument> args, IList<Option> options)
#pragma warning restore 1998
            {
                Assert.Equal(2, args.Count);

                Assert.Equal("argument1", args[0].Name);
                Assert.Equal("abc123", args[0].Value);
                Assert.Equal("argument2", args[1].Name);
                Assert.Equal("abc321", args[1].Value);

                called = true;
                return "Yay, it works!";
            };

            result = await manager.Invoke(new List<string>()
            {
                "test",
                "abc123",
                "abc321"
            });

            Assert.Equal("Yay, it works!", result);
            Assert.True(called);
        }

        [Fact]
        public async void TestInvokeInvalidArgType()
        {
            ICommand cmd = new BasicCommand("test", "test description", new List<Argument>()
            {
                new Argument() { Name = "argument1", ExpectedValueType = typeof(Int32)}, new Argument() { Name = "argument2" }
            }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            var result = await Assert.ThrowsAsync<ArgumentException>(async () => await manager.Invoke(new List<string>()
            {
                "test",
                "abc123",
                "value"
            }));

            Assert.Equal("Invalid argument value. Expected value of argument1 to be of type Int32.", result.Message);
        }

        [Fact]
        public async void TestInvokeUnknownOption()
        {
            ICommand cmd = new BasicCommand("test", "test description", new List<Argument>()
            {
                new Argument() { Name = "argument1", ExpectedValueType = typeof(Int32)}, new Argument() { Name = "argument2" }
            }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            var result = await Assert.ThrowsAsync<ArgumentException>(async () => await manager.Invoke(new List<string>()
            {
                "test",
                "abc123",
                "123",
                "--apa"
            }));

            Assert.Equal($"Invalid command signature, unknown option 'apa'. Expected: {Manager.GetSignature(cmd)}", result.Message);
        }

        [Fact]
        public async void TestInvokeOptionValueMissing()
        {
            ICommand cmd = new BasicCommand("test", "test description", new List<Argument>() { }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            var result = await Assert.ThrowsAsync<ArgumentException>(async () => await manager.Invoke(new List<string>()
            {
                "test",
                "--option1",
                "--option2"
            }));

            Assert.Equal("Unexpected value of option. Expected value of 'option2' to be of type Int32. Value was empty.", result.Message);
        }

        [Fact]
        public async void TestInvokeOptionWrongValue()
        {
            ICommand cmd = new BasicCommand("test", "test description", new List<Argument>() { }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            });

            manager.RegisterCommand(cmd);

            var result = await Assert.ThrowsAsync<ArgumentException>(async () => await manager.Invoke(new List<string>()
            {
                "test",
                "--option1",
                "--option2=hej!"
            }));

            Assert.Equal("Invalid option value. Expected value of 'option2' to be of type Int32.", result.Message);
        }

        [Fact]
        public void TestGetCommands()
        {
            manager.RegisterCommand(new BasicCommand("test", "test description", new List<Argument>() { }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            }));
            manager.RegisterCommand(new BasicCommand("test2", "test description", new List<Argument>() { }, new List<Option>()
            {
                new Option() { Name = "option1", Alternatives = new List<string>() {"o", "p"}},
                new Option() {Name = "option2", Alternatives = new List<string>() { "f", "g"}, ExpectedValueType = typeof(Int32)}
            }));

            Assert.Equal(2, manager.Commands.Count);
            Assert.Equal("test", manager.Commands[0].Name);
            Assert.Equal("test2", manager.Commands[1].Name);
        }

    }
}