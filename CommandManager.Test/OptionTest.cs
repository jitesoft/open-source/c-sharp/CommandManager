﻿using System.Collections.Generic;
using Jitesoft.Commands;
using Xunit;

namespace Test
{
    public class OptionTest
    {
        [Fact]
        public void TestCreate()
        {
            var o = new Option()
            {
                Name = "TestName",
                Alternatives = new List<string>() {"a", "b", "c"},
                ExpectedValueType = typeof(string),
                Description = "TestDescription"
            };

            Assert.Equal("TestName", o.Name);
            Assert.Equal("TestDescription", o.Description);
            Assert.Equal(typeof(string), o.ExpectedValueType);
            Assert.Equal(3, o.Alternatives.Count);
            Assert.Equal("a", o.Alternatives[0]);
            Assert.Equal("b", o.Alternatives[1]);
            Assert.Equal("c", o.Alternatives[2]);
        }
    }
}