﻿using System.Collections.Generic;
using Jitesoft.Commands;
using Test.TestObjects;
using Xunit;

namespace Test
{
    public class CommandTest
    {
        [Fact]
        public void TestCreateCommand()
        {
            ICommand command = new BasicCommand("TestName", "TestDescription");
            Assert.Equal("TestName", command.Name);
            Assert.Equal("TestDescription", command.Description);
        }

        [Fact]
        public void TestCreateCommandWithArguments()
        {
            ICommand command = new BasicCommand("TestName", "TestDescription", new List<Argument>
            {
                new Argument {Name = "arg1"},
                new Argument {Name = "arg2"}
            });

            Assert.Equal(2, command.Arguments.Count);
            Assert.Equal("arg1", command.Arguments[0].Name);
            Assert.Equal("arg2", command.Arguments[1].Name);
        }

        [Fact]
        public void TestCreateCommandWithOptions()
        {
            ICommand command = new BasicCommand("TestName", "TestDescription", new List<Argument>
            {
                new Argument {Name = "arg1"},
                new Argument {Name = "arg2"}
            }, new List<Option>
            {
                new Option {Name = "opt1"},
                new Option {Name = "opt2"}
            });

            Assert.Equal(2, command.Arguments.Count);
            Assert.Equal("opt1", command.Options[0].Name);
            Assert.Equal("opt2", command.Options[1].Name);
        }
    }
}