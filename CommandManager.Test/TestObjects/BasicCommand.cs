﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Jitesoft.Commands;

namespace Test.TestObjects
{
    public class BasicCommand : ICommand
    {
        public delegate Task<string> OnHandleDelegate(IList<Argument> args, IList<Option> options);

        public OnHandleDelegate Callback;

        internal BasicCommand(string name = "", string description = "", IList<Argument> arguments = null,
            IList<Option> options = null)
        {
            Name = name;
            Description = description;
            Arguments = arguments ?? new List<Argument>();
            Options = options ?? new List<Option>();
        }

        public IList<Option> Options { get; }
        public IList<Argument> Arguments { get; }
        public string Name { get; }
        public string Description { get; }

        public async Task<string> HandleAsync(IList<Argument> args, IList<Option> options)
        {
            return await Callback(args, options);
        }
    }
}