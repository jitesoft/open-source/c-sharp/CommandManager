﻿using Jitesoft.Commands;
using Xunit;

namespace Test
{
    public class ArgumentTest
    {
        [Fact]
        public void TestCreate()
        {
            var arg = new Argument()
            {
                Name = "TestArgument",
                Description = "TestDescription",
                ExpectedValueType = typeof(string)
            };

            Assert.Equal("TestArgument", arg.Name);
            Assert.Equal("TestDescription", arg.Description);
            Assert.Equal(typeof(string), arg.ExpectedValueType);
        }
    }
}